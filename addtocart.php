<?php
session_start();
require_once("koneksi.php");

$ProdukID = $_GET["ProdukID"];
$pengguna = $_SESSION["username"];
$total_records = 0;

try{
    $stmt1 = $conn->prepare("SELECT Jumlah FROM cart where ProdukID=? and Pengguna=?");  
    $stmt1->bind_param("is", $ProdukID, $pengguna);
    $stmt1->execute();
    $stmt1->bind_result($jumlah);

    while($stmt1->fetch()){
        $total_records = $jumlah;
    }

    //echo $total_records." ".$ProdukID;
    if($total_records>0){
        $total_records+=1;
        $stmt = $conn->prepare("update cart set Jumlah=? where ProdukID=? and Pengguna=?");
        $stmt->bind_param("iis", $total_records,$ProdukID,$_SESSION["username"]);
        $stmt->execute();
        $pesan = "Proses update cart berhasil";
        echo $pesan;
        //header("Location:/ukdwstore/lihatcart.php?pesan=$pesan");
    }else {
        $jumlah = 1;
        $stmt = $conn->prepare("INSERT INTO cart (ProdukID,Jumlah,Pengguna) VALUES (?,?,?)");
        $stmt->bind_param("iis", $ProdukID,$jumlah,$_SESSION["username"]);
        $stmt->execute();
        $pesan = "Proses tambah cart berhasil";
        echo $pesan;
        //header("Location:/ukdwstore/lihatcart.php?pesan=$pesan");
    }
}catch(Exception $e){
    echo "Error :".$e->getMessage();
}
finally{
    $stmt->close();
    $conn->close();
}

?>