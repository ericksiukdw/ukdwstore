-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 21, 2018 at 01:32 PM
-- Server version: 10.1.8-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ukdwstoredb`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `CartID` int(11) NOT NULL,
  `ProdukID` int(11) NOT NULL,
  `Jumlah` int(11) NOT NULL,
  `Pengguna` varchar(50) NOT NULL,
  `Tanggal` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`CartID`, `ProdukID`, `Jumlah`, `Pengguna`, `Tanggal`) VALUES
(1, 3, 4, 'erick', '2018-05-15 08:38:25'),
(2, 2, 1, 'erick', '2018-05-15 08:38:41'),
(4, 1, 1, 'erick', '2018-05-21 18:05:59');

-- --------------------------------------------------------

--
-- Table structure for table `itemjual`
--

CREATE TABLE `itemjual` (
  `ItemJualID` int(11) NOT NULL,
  `IdNotaJual` int(11) NOT NULL,
  `ProdukID` int(11) NOT NULL,
  `Jumlah` int(11) NOT NULL,
  `HargaJual` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE `pengguna` (
  `username` varchar(50) NOT NULL,
  `kunci` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telp` varchar(50) NOT NULL,
  `aturan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`username`, `kunci`, `nama`, `email`, `telp`, `aturan`) VALUES
('argo', '2139b5432be141e3e83c4a77c0c6e14f', 'Argo Lawu', 'argo@gmail.com', '554433', 'member'),
('budi', '00dfc53ee86af02e742515cdcf075ed3', 'Budi Anduk', 'budi@gmail.com', '556677', 'member'),
('erick', 'ac43724f16e9241d990427ab7c8f4228', 'Erick Kurniawan', 'erick@si.ukdw.ac.id', '778899', 'member'),
('joko', '9ba0009aa81e794e628a04b51eaf7d7f', 'Joko Aja', 'joko@gmail.com', '776655', 'member');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE `penjualan` (
  `IdINotaJual` int(11) NOT NULL,
  `Username` varchar(50) NOT NULL,
  `TanggalBeli` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `AlamatKirim` text NOT NULL,
  `Telp` varchar(50) NOT NULL,
  `BuktiTransfer` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `ProdukID` int(11) NOT NULL,
  `NamaProduk` varchar(50) NOT NULL,
  `Deskripsi` text NOT NULL,
  `HargaBeli` bigint(20) NOT NULL DEFAULT '0',
  `HargaJual` bigint(20) NOT NULL DEFAULT '0',
  `Jumlah` int(11) NOT NULL DEFAULT '0',
  `Gambar` varchar(100) NOT NULL,
  `Tanggal` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`ProdukID`, `NamaProduk`, `Deskripsi`, `HargaBeli`, `HargaJual`, `Jumlah`, `Gambar`, `Tanggal`) VALUES
(1, 'APPLE MACBOOK PRO RETINA ME293 GARANSI ', 'APPLE MACBOOK PRO RETINA ME293 GARANSI \r\nGaransi International', 15000000, 16500000, 1, '5ade8b876c9f3macbook1.jpg', '2018-04-24 08:42:31'),
(2, 'Macbook Pro 15 inch Core i7 2011', 'Macbook Pro 15 inch Core i7 2011\r\nGaransi Apple Store Indonesia', 12000000, 13000000, 2, '5ade8be120c84macbook2.jpg', '2018-04-24 08:44:01'),
(3, 'Apple Ipad Mini II', 'Apple Ipad Mini 2', 7000000, 8000000, 10, '5af97177bb4f9ipad.jpg', '2018-05-14 18:22:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`CartID`);

--
-- Indexes for table `itemjual`
--
ALTER TABLE `itemjual`
  ADD PRIMARY KEY (`ItemJualID`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`IdINotaJual`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`ProdukID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `CartID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `itemjual`
--
ALTER TABLE `itemjual`
  MODIFY `ItemJualID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `penjualan`
--
ALTER TABLE `penjualan`
  MODIFY `IdINotaJual` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `ProdukID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
