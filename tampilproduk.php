<?php
require_once("koneksi.php");
require_once("headerpage.php");
?>

<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="index.php">Home</a>
    </li>
    <li class="breadcrumb-item active">Daftar Produk</li>
</ol>
<div class="row">
  
        <?php 
            $limit = 2;  
            if (isset($_GET["page"])) { 
                $page  = $_GET["page"]; 
            } else { 
                $page=1; 
            };
            
            $start_from = ($page-1) * $limit;  
            $sql = "SELECT ProdukID,NamaProduk,Deskripsi,HargaBeli,HargaJual,Jumlah,Gambar,Tanggal FROM produk ORDER BY NamaProduk ASC LIMIT ?,?";  
            $stmt = $conn->prepare($sql);
            $stmt->bind_param("ii", $start_from, $limit);
            $stmt->execute();
            $stmt->bind_result($ProdukID,$NamaProduk,$Deskripsi,$HargaBeli,$HargaJual,$Jumlah,$Gambar,$Tanggal);
        ?>
        <?php while($stmt->fetch()) { ?>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-3">
                        <img src="images/<?=$Gambar?>" width="100" />
                    </div>
                    <div class="col-md-9">
                        <strong><?=$NamaProduk?></strong>
                        <p><?=$Deskripsi?></p>
                        Harga Jual: <strong>Rp.<?=$HargaJual?></strong><br/>
                        <a href="addtocart.php?ProdukID=<?=$ProdukID?>" class='btn btn-success btn-sm'>Add To Cart</a>
                    </div>
                </div>
            </div>
        <?php } ?>
</div>
<div class="row">
        <?php 
            $stmt->close();
            
            $sqlpaging = "SELECT COUNT(ProdukID) FROM produk";  
            $result = $conn->query($sqlpaging);
            $row = $result->fetch_row();  
            $total_records = $row[0];  
            $total_pages = ceil($total_records / $limit);  
            $pagLink = "<ul class='pagination'>";  
            for ($i=1; $i<=$total_pages; $i++) {  
                         $pagLink .= "<li><a href='tampilproduk.php?page=".$i."'>".$i."</a></li>";  
            };  
            echo $pagLink . "</ul>";  
            $conn->close();
        ?>
        </div>
<?php 

require_once("footerpage.php");
?>